import { DecisionBooleanType } from '../../config/node.types';
import { VariableValues } from '../../workflowContext';

const decision = (config: DecisionBooleanType, state: VariableValues): number =>
  // EVALUATE AND REPLACE FUNCTIONS HERE
  state['userEmail'] === 'sergio@email.com' ? config.trueStep : config.falseStep;

export default decision;
