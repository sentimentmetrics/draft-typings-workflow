import { NodeType, UserInputType } from '../../config/node.types';
import { VariableType } from '../../config/variables.types';

export const getForm = (node: NodeType<UserInputType>, variables: VariableType[]) =>
  variables.find((variable) => variable.id === node.config.schemaId);

export const validateInput = (variableDefinition: VariableType, body: object) => {
  const variableDefinitionId = variableDefinition.id;
  const formIds = Object.keys(variableDefinition.schema).map(
    (key) => `${variableDefinitionId}.${key}`
  );
  let isFormValid = true;
  //! TODO: THIS IS SIMPLISTIC VALIDATION  FOR THE EXAMPLE
  formIds.forEach((formId) => {
    if (!Reflect.has(body, formId)) isFormValid = false;
  });
  return isFormValid;
};
