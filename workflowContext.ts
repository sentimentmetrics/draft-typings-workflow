export type WorkflowState = {
  id: string;
  agentId: string;
  workflowId: string;
  currentStep?: number;
  previousStep?: number;
  checkPoints?: Context[];
};

export type Context = {
  executedStep: number;
  variables?: VariableValues;
  createdAt: Date;
};

export type VariableValues = { [key: string]: string | number | Date | boolean };

// ILLUSTRATIVE ONLY
const Example: WorkflowState = {
  id: 'UUID',
  agentId: 'sergio',
  workflowId: 'workflow1',
  currentStep: 0,
  checkPoints: [
    {
      executedStep: 0,
      variables: {
        'form_id.id': 1,
        'form_id.firstName': 'sergio',
        'form_id.secondName': 'Miguel',
      },

      createdAt: new Date(),
    },
  ],
};
