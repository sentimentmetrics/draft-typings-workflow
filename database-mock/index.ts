import { join } from 'path';
import { Low, JSONFile } from 'lowdb';

import { WorkflowState } from '../workflowContext';
import { WorkFlowType } from '../config/workflowtypes';

const workflowDb = (() => {
  let dbInstance : Low<WorkFlowType[]>;

  return async () => {
    if (dbInstance) {
      return dbInstance;
    }

    const file = join(__dirname, 'workflows.json');
    const adapter = new JSONFile<WorkFlowType[]>(file);

    dbInstance = new Low(adapter);
    await dbInstance.read();
    dbInstance.data = dbInstance.data || [];

    return dbInstance;
  }
})();

const workflowContext = (() => {
  let dbInstance : Low<WorkflowState[]>;

  return async () => {
    if (dbInstance) {
      return dbInstance;
    }

    const file = join(__dirname, 'workflowStates.json');
    const adapter = new JSONFile<WorkflowState[]>(file);

    dbInstance = new Low(adapter);
    await dbInstance.read();
    dbInstance.data = dbInstance.data || [];

    return dbInstance;
  }
})();

const getDataBase = async () => ({
  workflows: await workflowDb(),
  state: await workflowContext(),
});

export default getDataBase;
