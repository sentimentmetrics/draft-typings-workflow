import express, { Express, Request, Response } from 'express';
import { uuid } from 'uuidv4';
import { Low } from 'lowdb';

import Database from './database-mock';
import { getForm, validateInput } from './components/USER_INPUT';
import HTTP_COMPONENT from './components/API_CALL';
import DECISION_COMPONENT from './components/DECISION_BOOLEAN';

import {
  NodeType,
  UserInputType,
  NodeTemplates,
  RequestType,
  DecisionBooleanType,
} from './config/node.types';
import { Context, VariableValues, WorkflowState } from './workflowContext';
import { WorkFlowType } from './config/workflowtypes';

const app: Express = express();
const port = 4000;

app.use(express.json());

app.get('/workflows', async (req: Request, res: Response) => {
  const dbs = await Database();
  const workFlow = dbs.workflows.data.map(({ id, name, description }) => ({
    id,
    name,
    description,
  }));
  return res.json(workFlow);
});

app.post('/new/:id', async (req: Request<{ id: string }>, res: Response) => {
  const dbs = await Database();
  const workFlow = dbs.workflows.data.find((wf) => wf.id === req.params.id);
  const wfState: WorkflowState = {
    id: uuid(),
    workflowId: workFlow.id,
    agentId: 'Sergio',
    currentStep: 1,
    checkPoints: [],
  };

  dbs.state.data.push(wfState);
  await dbs.state.write();
  return res.json(`/execute/${workFlow.id}/${wfState.id}`);
});

app.post(
  '/execute/:workflowId/:stateId',
  async (req: Request<{ workflowId: string; stateId: string }>, res: Response) => {
    const dbs = await Database();
    const workFlow = dbs.workflows.data.find((wf) => wf.id === req.params.workflowId);
    const state = dbs.state.data.find((state) => state.id === req.params.stateId);
    const stepMap: Map<number, NodeType<NodeTemplates>> = new Map();

    workFlow.nodes.forEach((node) => {
      stepMap.set(node.step, node);
    });

    let node = stepMap.get(state?.currentStep);

    while (node) {
      const hasResult = await processNode(node, workFlow, state, dbs.state, req);
      if (hasResult) return res.json(hasResult);
      node = stepMap.get(state.currentStep);
    }

    res.json('FINISHED');
  }
);

app.post(
  '/execute/:workflowId/:stateId/:rewind',
  async (req: Request<{ workflowId: string; stateId: string; rewind: number }>, res: Response) => {
    const dbs = await Database();
    const state = dbs.state.data.find((state) => state.id === req.params.stateId);

    const allRecords = state.checkPoints.filter(
      (rec) => rec.executedStep === Number(req.params.rewind)
    );
    const newState: Context = { ...allRecords.pop(), createdAt: new Date() };

    state.checkPoints.push(newState);
    state.currentStep = Number(req.params.rewind);
    await dbs.state.write();
    return res.json(`/execute/${req.params.workflowId}/${req.params.stateId}/`);
  }
);

const processNode = async (
  node: NodeType<NodeTemplates>,
  workFlow: WorkFlowType,
  state: WorkflowState,
  stateDb: Low<WorkflowState[]>,
  req: Request
): Promise<object | null> => {
  let stateDbRec = stateDb.data.find((s) => s.id === state.id);
  const checkpointLength = stateDbRec.checkPoints.length;
  let lastState: VariableValues = stateDbRec.checkPoints[checkpointLength - 1]?.variables || {};

  if (node.operation === 'USER_INPUT') {
    stateDbRec.checkPoints[stateDb.data.length - 1]?.variables || {};
    const form = getForm(node as NodeType<UserInputType>, workFlow.variables);
    if (!validateInput(form, req.body)) return form;
    stateDbRec.checkPoints.push({
      executedStep: node.step,
      createdAt: new Date(),
      variables: { ...lastState, ...req.body },
    });
    stateDbRec.currentStep = node.next;
    await stateDb.write();
  }

  if (node.operation === 'API_CALL') {
    const config = node.config as RequestType;
    let url = config.url;
    url = url.replace('$$form_id.id$$', lastState['form_id.id'] as string);
    const { data } = await HTTP_COMPONENT.get(url);
    const userEmail: string = data.data.email;

    stateDbRec.checkPoints.push({
      executedStep: node.step,
      createdAt: new Date(),
      variables: { ...lastState, userEmail },
    });
    stateDbRec.currentStep = node.next;
    await stateDb.write();
  }
  if (node.operation === 'DECISION_BOOLEAN') {
    const config = node.config as DecisionBooleanType;
    const nextStep = DECISION_COMPONENT(config, lastState);
    stateDbRec.currentStep = nextStep;
    await stateDb.write();
  }
};

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at https://localhost:${port}`);
});
