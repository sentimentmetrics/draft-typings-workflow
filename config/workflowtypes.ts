import { VariableType } from './variables.types';
import { NodeType, NodeTemplates, RequestType, DecisionBooleanType } from './node.types';

export type WorkFlowType = {
  id: string;
  name: string;
  description: string;
  creator: string;
  createdAt: Date;
  lastModifiedBy?: string;
  modifiedAt?: Date;
  isValid: boolean;
  inUse: boolean;
  isActive: boolean;
  tags: string[];
  allowedGroups?: string[];
  //CONFIG
  variables?: VariableType[];
  nodes?: NodeType<NodeTemplates>[];
};

const variables: VariableType[] = [
  {
    id: 'form_id',
    name: 'This is a Form',
    dataType: 'complex',
    description: 'This is a Description for this Form',
    schema: {
      firstName: { title: 'First Name', type: 'text' },
      secondName: { title: 'Second Name', type: 'text' },
      password: { title: 'PAssword', type: 'password' },
    },
  },
  {
    id: 'userEmail',
    name: 'The Email',
    dataType: 'email',
  },
];

const nodes: NodeType<NodeTemplates>[] = [
  {
    id: 'INITIAL_NODE',
    title: 'HTTP REQUEST',
    step: 0,
    operation: 'API_CALL',
    config: <RequestType>{
      url: 'https://reqres.in/api/users/$$form_id.firstName$$',
      next: 1,
    },
    next: 1,
    output: 'userEmail',
  },
  {
    id: 'SECOND',
    title: 'USER IS SERGIO ',
    parent: 0,
    step: 1,
    operation: 'DECISION_BOOLEAN',
    config: <DecisionBooleanType>{
      condition: '$$userEmail$$ == sergio@sentiment.io',
      trueStep: 2,
      falseStep: 3,
    },
  },
];

export const workflow1: WorkFlowType = {
  id: 'UNIQUE_WORKFLOW',
  name: 'My first WorkFlow',
  description: 'This workflow is the first of a kind',
  creator: 'PETER',
  createdAt: new Date(),
  lastModifiedBy: 'SERGIO',
  modifiedAt: new Date(),
  isValid: true,
  inUse: false,
  isActive: false,
  tags: ['first', 'the best'],
  allowedGroups: ['admins', 'ebay', 'sentiment'],
  variables,
  nodes,
};
