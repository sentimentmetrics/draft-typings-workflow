export type DataType = 'boolean' | 'date' | 'text' | 'number' | 'email' | 'password' | 'complex';

type SchemaField = {
  [id: string]: { title: string; type: Exclude<DataType, 'complex'> };
};

export type VariableType = {
  id: string;
  name: string;
  description?: string;
  dataType: DataType;
  defaultValue?: string | boolean;
  schema?: SchemaField;
};

const SampleVariable: VariableType = {
  id: 'form_id',
  name: 'This is a Form',
  dataType: 'complex',
  description: 'This is a Description for this Form',
  schema: {
    firstName: { title: 'First Name', type: 'text' },
    secondName: { title: 'Second Name', type: 'text' },
    password: { title: 'PAssword', type: 'password' },
  },
};
