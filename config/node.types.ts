type OperationType =
  | 'API_CALL'
  | 'DECISION_BOOLEAN'
  | 'PARALLEL'
  // | 'SUB_WORKFLOW'
  | 'FORMATTER'
  | 'USER_INPUT'
  | 'MULTI_DECISION'

export type ParallelType = {
  steps: number[];
};

export type DecisionBooleanType = {
  trueStep: number;
  falseStep: number;
  condition: string;
};

export type MultipleDecision = {
  steps: number[]
  condition: string;
};





export type RequestType = {
  url: string;
  headers?: any;
  body?: any;
};

export type UserInputType = {
  schemaId: string;
};

//! TODO: TO THINK SUB WORKFLOW
export type SUB_WF_TYPE = {
  WF_ID: string;
  output?: string;
};

export type NodeTemplates =
  | RequestType
  | DecisionBooleanType
  | ParallelType
  | UserInputType;
//PASSING VARIABLES COULD BE SOMETHING LIKE :USER_ID_VARIABLE_OF_CONTEXT
// COMES FROM WORKFLOW VARIABLES
// $$VARIABLE_COMES$$

export type NodeType<T> = {
  id: string;
  title: string;
  step: number;
  next?: number;
  parent?: number;
  operation: OperationType;
  config: T;
  output?: string;
};

const paralel: NodeType<ParallelType> = {
  id: 'DDD',
  title: 'DDD',
  operation: 'PARALLEL',
  step: 0,
  next: 4,
  config: {
    steps: [1, 2, 3],
  },
};

const request: NodeType<RequestType> = {
  id: 'DDD',
  title: 'DDD',
  operation: 'API_CALL',
  step: 0,
  next: 4,
  config: {
    url: 'http://www.gogole.com/$$MY_VARIABLE_ID$$',
    body: {
      firstName: '$$form_id.firstName$$',
    },
  },
};
